package org.dcs.tools.vos;


import java.io.Serializable;

public class ResponseVo<T> implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7093102744340845189L;
	

	public ResponseVo(int statusCode, String statusMessage, T data) {
		super();
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
		this.data = data;
	}
	private int statusCode;
	private String statusMessage;
	private T data;
	
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}


}
