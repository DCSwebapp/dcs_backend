package org.dcs.tools.exceptions;

public class ResourceNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1911877106938100541L;

	private String errorMessage;
	 
	public String getErrorMessage() {
		return errorMessage;
	}
	public ResourceNotFoundException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	public ResourceNotFoundException() {
		super();
	}
}
