package org.dcs.tools.dao;

import org.hibernate.Session;

public interface ParentDao {
	
	Session getSession();

}
