package org.dcs.tools.dao.impl;

import org.dcs.tools.dao.ParentDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ParentDaoImpl implements ParentDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public Session getSession(){
		Session session = sessionFactory.getCurrentSession();
		System.out.println("not recommended");
		return session;
	}
}
