package org.dcs.tools.rest;


import org.dcs.tools.exceptions.ResourceNotFoundException;
import org.dcs.tools.vos.ErrorResponseVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
@ControllerAdvice
public class ResourceNotFoundExceptionHandler {
	Logger logger = LoggerFactory.getLogger(ResourceNotFoundExceptionHandler.class);
	
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<ErrorResponseVo> resourceNotFoundExceptionHandler(Exception ex) {
		logger.error("exception :",ex);
		ErrorResponseVo error = new ErrorResponseVo();
		error.setStatusCode(HttpStatus.NOT_FOUND.value());
		error.setStatusMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponseVo>(error, HttpStatus.NOT_FOUND);
	}
	
	
	
}
