package org.dcs.tools.rest.api;

import org.dcs.tools.exceptions.ResourceNotFoundException;
import org.dcs.tools.models.Mapping;
import org.dcs.tools.services.MappingService;
import org.dcs.tools.vos.MappingRequestVo;
import org.dcs.tools.vos.MappingResponseVo;
import org.dcs.tools.vos.ResponseVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

public class EndPointValidation {

	
	@Autowired
	private MappingService mappingService;

	Logger logger = LoggerFactory.getLogger(MappingRestApi.class);

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody ResponseVo<MappingResponseVo> create(@RequestBody MappingRequestVo mapVo) throws ResourceNotFoundException {
		logger.info("entering method abc");		
		Mapping createdUser =  mappingService.createUser(mapVo);		
		return new ResponseVo<MappingResponseVo>(200,"success",new MappingResponseVo());
	
	}
	
}
