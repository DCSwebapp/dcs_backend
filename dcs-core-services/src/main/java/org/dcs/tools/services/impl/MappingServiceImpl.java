package org.dcs.tools.services.impl;



import org.dcs.tools.models.Mapping;
import org.dcs.tools.services.MappingService;
import org.dcs.tools.vos.MappingRequestVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MappingServiceImpl implements MappingService{

	Logger logger = LoggerFactory.getLogger(MappingServiceImpl.class);
	@Transactional
	public Mapping createUser(MappingRequestVo mapper)  {
		System.out.println("@start create");
		return new Mapping();
	}
}
