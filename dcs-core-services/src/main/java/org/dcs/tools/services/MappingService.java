package org.dcs.tools.services;

import org.dcs.tools.exceptions.ResourceNotFoundException;
import org.dcs.tools.models.Mapping;
import org.dcs.tools.vos.MappingRequestVo;

public interface MappingService {

	Mapping createUser(MappingRequestVo mapper) throws ResourceNotFoundException;
	
	
}
